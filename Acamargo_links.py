import requests
from bs4 import BeautifulSoup
import csv

# Cria o CSV para ser inserido o nome e o link das peças
arquivo = open('links_acamargo_mongo.csv', mode='w')
arquivo = csv.writer(arquivo, delimiter=';')


for indice in range(1, 155):
    pagina = requests.get('https://www.acamargo.com/pecas-para-tratores.html?p=%d' % indice)
    conteudo = pagina.content
    pagina_soup = BeautifulSoup(conteudo, 'lxml')

    produtos = pagina_soup.find_all('h2',class_='product-name')
    for produto in produtos:
        link = produto.a
        link_produto = link['href']
        nome_produto = produto.text
        arquivo.writerow([nome_produto, link_produto])
    print('ESTOU NO INDICE:', str(indice))
