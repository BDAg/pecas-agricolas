Projeto integrador do 1º semestre de 2019

Membros:
Felipe Jonas 1º Termo
Andrew 1º Termo
Augusto 1º Termo
Gabriel 1º Termo
Maria Eduarda 1º Termo
Renan 1º Termo
William 1º Termo
Fábio 1º Termo
Renan Zulian 5º Termo
Christopher 5º Termo

# Bem vindo ao projeto PeçasAgro
Neste projeto irmeos coletar dados de peças em web sites e armazenar em um banco de dados.

## Requerimentos
- **Repositório**
    - [GitLab](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#git-setup)
- **Web Bots**
    - [Python 3](https://gitlab.com/BDAg/Localiza_Perfil_Agro#python-3)
    - [PyMongo](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#pymongo)
    - [Requests](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#requests)
    - [Beautiful Soup](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#beautiful-soup)
- **Banco de Dados**
    - [NoSQLBooster](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#NoSQLBooster)
    - [MongoDB Atlas](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#mongodb-atlas)
    - [NoSQLBooster e Conexão MongoDB Atlas](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#NoSQLBooster-e-Conexão-MongoDB-Atlas)

## Git setup
### 1. DOWNLOAD DO GIT NA MAQUINA:
**site oficial:**
https://about.gitlab.com/installation/#ubuntu

**Instalação:**
>sudo apt-get update

>sudo apt-get install -y curl openssh-server ca-certificates

>sudo apt-get install -y postfix

>curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

>sudo EXTERNAL_URL="http://gitlab.example.com" apt-get install gitlab-ee

>sudo apt install git

### 2. CONFIGURAÇÃO NA MAQUINA:

>git config --global user.name "Meu nome"

>git config --global user.email "emaildapessoa@email.com"

>ssh-keygen -t rsa -b 4096 -C "emaildapessoa@email.com" ( esse vai gerar sua chave em ~/.ssh)

>cat ~/.ssh/id_rsa.pub (vai trazer um código, copia tudo)
(salva a nova chave lá no gitlab)

### 3.A) CRIAR A PARTIR DE UM PROJETO (PASTA) EXISTENTE:

>cd <caminho_da_pasta_onde_quer_incluir_seu_repositorio>

>git clone git@gitlab.com:grupo/nome_do_projeto.git

### 3.B) CRIAR DE REPOSITORIO NOVO:

>cd existing_folder

>git init

>git remote add origin (caminho ssh do repositório remoto, exemplo: git@gitlab.com:grupo/nome_do_projeto.git)

>git add .

>git commit -m "commit inicial"

>git push origin master

## Git comandos
- **Upload**
	>git add .
	
    >git commit -m "mensagem"
	
    >git push origin master

- **Download**
	>git pull origin master
	
    >git clone 'link do site''

## Python 3
**site oficial:**
https://www.python.org/about/gettingstarted/

## PyMongo
**site oficial:**
http://api.mongodb.com/python/current/

**Instalação:**
>sudo python3 -m pip install pymongo

## Requests
**site oficial:**
http://docs.python-requests.org/en/master/

**Instalação:**
>sudo apt install python3-pip

>pip3 install --upgrade pip

>pip3 install requests

## Beautiful Soup
**site oficial:**
https://www.crummy.com/software/BeautifulSoup/bs4/doc/

**Instalação:**
>sudo apt-get  install  python3-bs4

ou

>easy_install3 beautifulsoup4

ou

>pip3 install  beautifulsoup4

## Banco de dados 
### NoSQLBooster

**site oficial:**
https://nosqlbooster.com

***Download:***
https://nosqlbooster.com/downloads

### MongoDB Atlas

**Site oficial:**
https://www.mongodb.com/cloud/atlas
>Descer na página até o botão "Get started for free";
>
>Clicar nele para iniciar a criação de contas, colocando informações como e-mail, primeiro e ultimo nomes e uma senha;
>
>Após criar a conta, irá ser aberta uma área de criação de cluster's, pode ser cancelada;

### Conexão MongoDB Atlas e o NoSQLBooster
**Conectar no servidor**
>Passar o e-mail utilizado na criação da conta para os adm's do servidor;

**No site do servidor**
https://www.mongodb.com/cloud/atlas
>Após realizar o login na conta deve conectar na cluster do grupo AgroProspects;
>Na cluster, clicar em "Connect";
>Clicar na aba "Connect your aplication";
>Clicar na aba "Connect your aplication";
> Clicar em "I'm using driver 3.6 or later";
> Copiar a linha de programa que aparece;

**No NoSQLBooster**
>Ir em "Connect";
>Ir em "New Connection";
>Clicar em "From URI";
>Colar a linha de código alterando o escrito "password" e os sinais de maior e menor do lado para nossa senha do projeto;

    