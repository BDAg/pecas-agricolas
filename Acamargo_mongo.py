# Lê o arquivo csv, todas as linhas e as colunas 0 e 1(nome e link), insere no banco
import csv
from pymongo import MongoClient

conexao = MongoClient("mongodb+srv://bot1:agropecas123@pecas-ewnwj.mongodb.net/test?retryWrites=true")
myDb = conexao['pi-pecas-agro']

arquivo = open('links_acamargo.csv', mode='r')
arquivo = csv.reader(arquivo, delimiter=';')

for linha in arquivo:
    myDb.Link_Acamargo.insert({
        'nome': linha[0],
        'link': linha[1],
        'fonte': 'https://www.acamargo.com/',
        'coletado': False
    })
    print(linha[0] + ' OK')
    