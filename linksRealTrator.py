import requests
from bs4 import BeautifulSoup
import csv
from pymongo import MongoClient

conexao = MongoClient("mongodb+srv://bot1:agropecas123@pecas-ewnwj.mongodb.net/test?retryWrites=true")
myDb = conexao['pi-pecas-agro']
count = 1
while count<=80:
    page = requests.get("https://www.realtrator.com.br/pecas-para-tratores?page=%d"%count)
    contents = page.content
    print(count)
    soup = BeautifulSoup(contents, 'html.parser')
    for item in soup.find_all('div',class_='product-list-item-inner'):  
        nomeProduto = item.find('div',class_='product-name')
        nomeProduto = nomeProduto.a.h2.text

        link = item.find('div', class_="product-image")
        link = link.a['href']
        linkColeta = "https://www.realtrator.com.br"+link
        
        myDb.LinkRealTrator.insert_one({
        "nome" : str(nomeProduto),
        "link" : str(linkColeta),
        "fonte" : "https://www.realtrator.com.br/",
        "coletado" : False
        })
    count+=1


