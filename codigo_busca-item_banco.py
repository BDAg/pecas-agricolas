# Lê o arquivo csv, todas as linhas e as colunas 0 e 1(nome e link), insere no banco
import requests
from bs4 import BeautifulSoup
import csv
from pymongo import MongoClient

conexao = MongoClient("mongodb+srv://bot1:agropecas123@pecas-ewnwj.mongodb.net/test?retryWrites=true")
myDb = conexao['pi-pecas-agro']

busca_mongo_perfil = myDb.Link_Acamargo.find({'coletado': False},no_cursor_timeout=True)
for dado in busca_mongo_perfil:
	link = str(dado['link'])
	idMongo = (dado['_id'])
	# print(link)
	# Coleta os dados de cada link buscado no banco
	pagina = requests.get(link)
	conteudo = pagina.content
	pagina_soup = BeautifulSoup(conteudo, 'lxml')
	# print(conteudo)
	# print('--------------------------------------------------------')

	produtos = pagina_soup.find_all('div',class_='product-name')
	for produto in produtos:
		# print(produto)
		produto = produto.h1
		nome_produto = produto.text
		# print(nome_produto)
		if pagina_soup.find('p', class_='availability in-stock'):
			disponibilidade = pagina_soup.find('p', class_='availability in-stock').text
			# print(disponibilidade)
		else:
			disponibilidade = 'Não disponível'	
			# print(disponibilidade)
		if pagina_soup.find('div', class_='price-box'):
			preco = pagina_soup.find('span', class_='price')
			preco = preco.text.strip()
			preco = preco.replace("R$", "")
			preco = preco.replace(",", "")
			preco = preco.replace(".", "")
			lista = list()
			for precoFor in preco:
				lista += [precoFor]
			lista = lista[::-1]
			precoFor2 = ""
			forV = 1
			for precoFor in lista:
				if forV == 2:
					precoFor = precoFor+"."
				precoFor2 += precoFor
				forV +=1
			preco = precoFor2[::-1]

			# print(preco)
		else:
			preco = 0
		if pagina_soup.find('div', class_='std'):
			descricao_produto = pagina_soup.find('div', class_='std').text
			# print(descricao_produto)
		else:
			descricao_produto = 'Sem descrição'
	# Insere no banco os dados capturados de cada página
	myDb.pecas.insert_one({
		'nome': nome_produto,
		'descricao':descricao_produto,
		'preco':float(preco),
		'disponibilidade': disponibilidade,
		'link': link,
		'fonte': 'https://www.acamargo.com/',
	})
	print(nome_produto + ' OK')
# faz um update nos links Acamargo de False para True
	updateMongo = myDb.Link_Acamargo.update_one(
		{'_id':idMongo},
		{'$set':
			{'coletado':True}})
	print(updateMongo)