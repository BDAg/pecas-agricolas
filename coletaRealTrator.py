import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient

conexao = MongoClient("mongodb+srv://bot1:agropecas123@pecas-ewnwj.mongodb.net/test?retryWrites=true")
myDb = conexao['pi-pecas-agro']

buscaLink = myDb.LinkRealTrator.find({'coletado': False},no_cursor_timeout=True)
print(buscaLink)

conta = 0
for dado in buscaLink:
    link = str(dado['link'])
    
    entrar = requests.get(link)
    conteudo = entrar.content
    pecasEntrar = BeautifulSoup(conteudo, 'html.parser')
   
    for pecas in pecasEntrar.find_all('div', class_="element-content"):
        conta += 1

        print('----------------------------------------------------------------------------------------------------------------------------------------')
        print(conta)
        print(link)
        nome = "Indisponivel"
        codigo = "Indisponivel"
        printCategoria = "Indisponivel"
        preco = 0
        descricao = "Indisponivel"

        # nome
        if pecas.find('h1', class_="product-name") != None:
            nome = pecas.find('h1', class_="product-name")
            nome = nome.text
        
        # codigo
        if pecas.find('p', class_="product-sku") != None:
            codigo = pecas.find('p', class_="product-sku")
            codigo = codigo.span.text

        # Categoria
        if pecas.find('p', class_="product-category") != None:
            categoria = pecas.find('p', class_="product-category")
            categoriaTexto = categoria.select('a span')
            printCategoria=''
            for forCategoria in categoriaTexto:
                printCategoria += forCategoria.text+' '
        
    
    if pecasEntrar.find_all('p', class_="product-new-price")!=None:
        for preco in pecasEntrar.find_all('p', class_="product-new-price"):
            preco = preco.find('span', class_="product-big-price")
            preco = preco.text
            preco = preco.replace("R$ ", "")
            preco = preco.replace(",", "")
            preco = preco.replace(".", "")
            lista = list()
            for precoFor in preco:
                lista += [precoFor]
            lista = lista[::-1]
            precoFor2 = ""
            forV = 1
            for precoFor in lista:
                if forV == 2:
                    precoFor = precoFor+"."
                precoFor2 += precoFor
                forV +=1
            preco = precoFor2[::-1]
            
    
    for descricao in pecasEntrar.find_all('div', class_="product-view element-product-description"):
        descricao = descricao.find('div', class_="product-view-content")
        descricao = descricao.text
    
    
    myDb.pecas.insert_one({
    "Nome" : str(nome),
    "Link" : str(link),
    "Fonte" : str('https://www.realtrator.com.br/'),
    "Código" : str(codigo),
    "Categorias" : str(printCategoria),
    "Preco" : float(preco),
    "Descricao" : str(descricao)
    })
    myDb.LinkRealTrator.update_one({'link': link},{'$set':{'coletado':True}})